# Primitive Shapes Generator

Points clouds generator of basic primitive shapes.

This python library allows the creation of Point Clouds files, containing one or more geometrical primitives, under different modifiers.
The points are generated using primitive shapes as model.
Given a primitive parameters, the points are randomly sampled on its surface.
The result will be similar to a scan of a real world object with a shape approximable to a primitive.

<img src="pics/complete_scene.png"
     align="right"
     width="400" height="400" />
     
The aim is to make available a library to easily generate custom datasets suited for being used in tasks like shapes identification, primitive fitting, and robotic grasping.

As for now, it's possible to generate spheres, boxes and cylinders.

All the shapes can be defined by their position and rotation, and their size parameters (e.g. height, width, and depth in the case of the box).
The number of points composing the shapes can be set, and all the points are associated by a normal computed according the point position in the shape.


The modifiers currently available allows to:

- introduce **error** in the shape point, in the form of random perturbation of each point composing the shape;
- remove **part** of the shape, starting roughly from a bottom corner of the shape;
- add **noisy** points in the scene, in the form of random points, with random normals;

Once a scene is created and populated by shapes, it can be saved as **pcd** files, which contains both the points coordinates and the normals associated to the points.

Except commonly used libraries, like *math* and *numpy*, usually available on most systems, this library doesn't use any specific dependency.
It works with a Python 3.x interpreter.

## Performance
This library is designed to create all the points and apply all the modifications only when a primitive or a scene is saved.
When a scene or a primitive is saved, each point is generated individually, modified, saved on the disk, and deleted right afterwards.
This means that primitives with a very high number of points and/or scenes with high number of primitives can be generated quickly and, most importantly, without being limited on the RAM space.

# Usage
In order to use the library the first step is import it
```python
import primitive_shapes_generator as psg
```
then you can create any shape available just instantiating the relative class, using the following constructors:
- **Sphere**(*n_points, radius*)
- **Box**(*n_points, height, width, depth*)
- **Cylinder**(*n_points, height, radius*)

For instance, the following instruction
```python
b = psg.Box(1000,9,4,1)
```
will create a [monolith](https://en.wikipedia.org/wiki/Monolith_(Space_Odyssey)), namely a box of height 9, width 4, and depth 1.

Any shape can be translated and/or rotated using the following methods:
- **translate**(*self,new_center*) where *new_center* is either a list or a tuple of 3 floats;
- **rotate**(*self,quat=None*) where *quat* is a quaternion. If no quaternion is given, a random rotation is performed;

Rotations are performed using [quaternions](https://en.wikipedia.org/wiki/Quaternion). 
A built-in minimal quaternion data structure is available whithin this library. 
It can be instantiated either directly usign the following constructor:
```python
quaternion(x=0.0, y=0.0, z=0.0, w=1.0)
```
or using the convenience static funciton *fromEuler* that returns a quaternion from [Eulerian angles](https://en.wikipedia.org/wiki/Euler_angles):
```python
quaternion.fromEuler(pitch, yaw, roll, inRadian=True)
```
the angles used must be in [radians](https://en.wikipedia.org/wiki/Radian), unless the flag *inRadian* is explicitely set at *False*.

Following the example above, the following instructions:
```python
import math as mt
q = psg.quaternion.fromEuler(mt.pi/2.0, 0.0, 0.0)
b.rotate(q)
```
will rotate the monolith of 90 deg around the *y* axis.

A shape can be saved in any path with the *save* function
```python
b.save("/tmp/rotated_monolith.pcd")
```
which will generate the relative *pcd* file in the */tmp/* directory.
The *save* function use the PCD format as default.
If you want to save a XYZ format, you can specify it as file extension *".xyz"* insted of *".pcd"*.
The point cloud will be saved with the normals associated to each point by default.
In case you don't need the normals information you can set the flag *save_normals=False* in the *save* function.

Besides the aforementioned functions, every shape type comes with a set of utility geometrical functions like:

- **getVolume** which returns the volume of the shape;
- **getSurfaceDistance** which takes a point as input and returns the distance from that point to the closest part of the shape surface;
- **isPointOnSurface** which takes a point as input and returns 0 if the point lies on the shape surface, -1 if it is internal or 1 if it is external;

Besides these generic functions, there are also some type-specific utility functions.
For instance, boxes offer utility functions such as *getFaceCenters* and *getVertices*, whilst the principal axis of a cylinder can be found using the *getMainAxis* function.

# Modification Operators
In order to simulate effectively different type of real scan issues, a set of modificators is available. 

Multiple modifications can be applied on the same shape, however since the application order *does* matter, when the modifications are applied the library follows this order: 
```math
partial\to error\to rotation\to translation\to noise (scene)
```
## Partial Shape
It is possible to cut part of the shape using the *remove_part* function. 
This is useful to simulate a partial knowledge of the object.
So, this instruction:
```python
b.remove_part(0.25)
```
will remove around 25% of the shape.

It's important to note that this operation will not reduce the number of points.
All the points removed this way will be re-sampled in the available part.

## Sampling Error
Normally, all the points are sampled exactly on the shape surface.
It's possible to introduce a degree of error on the sampling using the *add_error* method.
The error can be relative to the size of the shape along each Cartesian axis, absolute, or a combination of both.
Using this method, each point *p* of the shape is modified as follows:
```math
p_i=p_i+\delta_i\qquad \delta_i\sim U(-d_i, d_i)\qquad d_i=(M_i-m_i)e_p+e_a
```
where $`M_i`$ and $`m_i`$ are the maximum and minimum value of all points *p* in the shape, $`e_p`$ and $`e_a`$ are the *error_percentage* and *error_absolute* parameters of the method and $`U`$ is the uniform distribution.

For instance, this instruction:
```python
b.set_error(error_percentage=0.1)
```
perturbate each point of the monolith of a random value $`\leq10`$% along all axes.

# Scene and Multiple Shapes
A *Scene* need to be instantiated to combine multiple shapes in the same points cloud:
```python
scene = psg.Scene()
```
Optionally, the *Scene* constructor can also accepts an initial shape that will be directly added to the new scene.

<img src="pics/mickey_mouse.png"
     align="left"
     width="300" height="290" />
     
Afterwards, any number of shapes can be added to the scene:
```python
scene.add_shape(b)
s1 = psg.Sphere(1000, 1)
s1.translate([1, 2, -3.4])
scene.add_shape(s)
s2 = psg.Sphere(1000, 3.5)
s2.translate([2, -9.3, 2.7])
scene.add_shape(s2)
```
The scene can then be saved similarly as before:
```python
scene.save("/tmp/multiple_shapes.pcd")
```
Any shape with any kind of modifier can be added in a scene, therefore it is possible to compose quite complex scenes very easily using just primitives.
Also, it's possible to rotate the whole scene using the *rotate* scene function and add an error to all the shapes included in a single call with the *set_error* function.

Snowman scene with 20% error | Snowman scene with 10% error | Snowman scene with no error
:----:|:----:|:----:
![](pics/snowman_02.png)|![](pics/snowman_01.png)|![](pics/snowman.png)

As example, the functions *make_mickey_mouse_scene* and *make_snowman_scene* are included in the **scene_collection.py** file, which accept no parameters and returns the scenes visible in the pictures.

## Add Noise
Random noise can be added to a scene in the form of random points with random normals sampled uniformly whithin the scene boundaries.
The scene boundaries are defined by the maximum and minimum values of the points present in the scene, along the 3 Carthesian axes.
The noise can be added using the following function:
```python
add_noise(n_noisy_points, enlarging_boundaries_factor=1.0, custom_noise_box=None)
```
where *n_noisy_points* is the number of noisy points to be added.

The optional parameter *enlarging_boundaries_factor* is used to extend the noise out the scene boundaries.
For instance a *enlarging_boundaries_factor=1.2* will generate noisy points up to 20% out the scene boundaries, along each axis.
This is particularly useful if the scene presents a single shape.
For instance, if the scene contains just an unrotated cube (that is a box with height=width=depth), without setting this parameter to a value greater than 1, all the noise will be generated inside the cube.

The optional parameter *custom_noise_box* accepts values in the form of $`((m_x,m_y,m_z), (M_x,M_y,M_z))`$ where $`m_i`$ and $`M_i`$ defines the minimum and maximum points of a diagonal of a custom box.
If this parameter is set, all the noise is included only within this custom box.

Since the noise is related to the scene boundaries, it's not possible to directly add noise to a certain shape.
In this case, the shape must be included in a scene first, then the noise can be added.

# Single Primitives Collection
A separate file *primitive_collection_generator.py* contains the procedures to generate a colelction of point clouds containing a single primtive under different modifications.


A collection of 591 shapes are presented under different conditions.
The shapes considered are the following:
- a number of 180 spheres with radius variable from 1 to 10 with steps of 0.05
- a number of 220 boxes with random rotation and width, heigh and depth variable from 1 to 10 with steps of 1
- a number of 190 cylinders with random rotation, radius from 0.5 to 5 with steps of 0.25 and heigh variable from 1 to 10 with steps of 1

All the shapes are centered in the origin.
Each collection type is formed by the same shapes (i.e. with exactly the same size and rotation) in the form of point clouds of 1000 points, with the following modifications:
- whole shapes with no noise or error added (clean)
- the same shapes with extra *noisy_points* noisy points randomly added in the scene (noisy)
- the same shapes where each point is perturbed in a random direction of a *e_perc* error (error)
- the same shapes with about *p_perc* of it removed (partial)
- all the possible combination of the aforementioned modifications

Every collection type is stored in a dedicated subdirectory with a specific name.
As said in the sections above, the added error simulate a sampling measurement inaccuracies, the added noise simulate extra points added as sampling errors and the partiality of the scene simulate an object occlusion or a partial scan.

The shapes parameters are saved in a shapes.csv file in each point clouds directory.

## Usage
The collections generation can be started invoking the file from the shell with some parameters:
```sh
$ ./primitive_collection_generator.py <target_dir> <collection_type>
```
where *<target_dir>* is the directory where the collecitons will be saved, and *<collection_type>* is the parameter that indicate the collection type to save.
Available parameters are:
- *-clean* The collection with complete shapes with no error and no noise
- *-noisy noisy_points* The collection with complete shapes with no error and *n_points* extra noise points
- *-error e_perc* The collection with complete shapes with an error percentage of *e_perc* and no noise
- *-partial p_perc* The collection with partial (*p_perc*%) shapes with no error and no noise
- *-all* All the collections above, and combination thereof, with *n_points=500*, *p_perc=0.5* and two different error variants of *e_perc=0.1* and *e_perc=0.2*
- *-seed s* A set can optionally be set to generate exactly the same point clouds (i.e. exactly same points) every time
- *-h* It prints an help message and quit

Multiple options can be used, and each collection will be saved in a separate subdirectory of *<target_dir>*.
If multiple options are used, a single collection type will be generated using a combination of modifications specified.
For instance, the following command:
```sh
$ ./primitive_collection_generator.py /tmp/ -error 0.2 -partial .3
```
will generate a folder */tmp/partial_error* containing primitives with 0.2 error and 0.3 partisl.