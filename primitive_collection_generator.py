#!/usr/bin/env python

'''
Code developed by Luca Baronti (lbaronti[at]gmail.com)
'''

import os, sys, argparse, copy
import numpy.random as nr
import primitive_shapes_generator as s_generator

# Some parameters used in the collection generation (min, max, step)
general_pars={	"n_points":1000, 
				"test_shapes":20,
				"default_error":.1,
				"default_noise":.5,
				"default_partial":.5
				}
sphere_pars={"radius": (1.0,10.0,0.05)}
box_pars={	"height": (1.0,10.0,1.0),
			"width": 	(1.0,10.0,1.0),
			"depth": 	(1.0,10.0,1.0)
			}
cylinder_pars={	"height": (1.0,10.0,1.0),
				"radius": 	(0.5,5.0,0.25)
				}
# Derived parameters
v=1.0
for s in [(sphere_pars[k][1]-sphere_pars[k][0])/sphere_pars[k][2] for k in sphere_pars.keys()]:
	v*=s
sphere_pars["number"]=v
v=1.0
for b in [(box_pars[k][1]-box_pars[k][0])/box_pars[k][2] for k in box_pars.keys()]:
	v*=b
box_pars["number"]=v
v=1.0
for c in [(cylinder_pars[k][1]-cylinder_pars[k][0])/cylinder_pars[k][2] for k in cylinder_pars.keys()]:
	v*=c
cylinder_pars["number"]=v

def make(dir_path, ref_spheres, ref_boxes, ref_cylinders):
	if not os.path.exists(dir_path):
		os.makedirs(dir_path)
	current_dir_path=os.path.join(dir_path,"spheres")
	if not os.path.exists(current_dir_path):
		os.makedirs(current_dir_path)
	f=open(os.path.join(current_dir_path,"shapes.csv"),'w')
	f.write("file_name,"+s_generator.Sphere.csv_labels()+"\n")
	for el in ref_spheres:
		el[1].save(os.path.join(current_dir_path,el[0]+".pcd"))
		f.write(el[0]+","+el[1].shapes[0].csv()+"\n")
	f.close()
	current_dir_path=os.path.join(dir_path,"boxes")
	if not os.path.exists(current_dir_path):
		os.makedirs(current_dir_path)
	f=open(os.path.join(current_dir_path,"shapes.csv"),'w')
	f.write("file_name,"+s_generator.Box.csv_labels()+"\n")
	for el in ref_boxes:
		el[1].save(os.path.join(current_dir_path,el[0]+".pcd"))
		f.write(el[0]+","+el[1].shapes[0].csv()+"\n")
	f.close()
	current_dir_path=os.path.join(dir_path,"cylinders")
	if not os.path.exists(current_dir_path):
		os.makedirs(current_dir_path)
	f=open(os.path.join(current_dir_path,"shapes.csv"),'w')
	f.write("file_name,"+s_generator.Cylinder.csv_labels()+"\n")
	for el in ref_cylinders:
		el[1].save(os.path.join(current_dir_path,el[0]+".pcd"))
		f.write(el[0]+","+el[1].shapes[0].csv()+"\n")
	f.close()

def make_clean(base_path, ref_spheres, ref_boxes, ref_cylinders):
	if not os.access(base_path, os.W_OK):
		raise ValueError("Can't write on "+base_path)
	dir_path=os.path.join(base_path,"clean")
	print("A clean collection will be generated on",dir_path)
	make(dir_path, ref_spheres, ref_boxes, ref_cylinders)

def add_noise(noisy_perc, ref_spheres, ref_boxes, ref_cylinders):
	ref_spheres_alt=[]
	for sphere in ref_spheres:
		s=copy.deepcopy(sphere[1])
		s.add_noise(int(general_pars["n_points"]*noisy_perc),1.1)
		ref_spheres_alt+=[(sphere[0],s)]
	ref_boxes_alt=[]
	for box in ref_boxes:
		b=copy.deepcopy(box[1])
		b.add_noise(int(general_pars["n_points"]*noisy_perc),1.1)
		ref_boxes_alt+=[(box[0],b)]
	ref_cylinders_alt=[]
	for cyl in ref_cylinders:
		c=copy.deepcopy(cyl[1])
		c.add_noise(int(general_pars["n_points"]*noisy_perc),1.1)
		ref_cylinders_alt+=[(cyl[0],c)]
	return ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt

def remove_part(partial_perc, ref_spheres, ref_boxes, ref_cylinders):
	ref_spheres_alt=[]
	for sphere in ref_spheres:
		s=copy.deepcopy(sphere[1])
		s.shapes[0].remove_part(part_perc=partial_perc)
		ref_spheres_alt+=[(sphere[0],s)]
	ref_boxes_alt=[]
	for box in ref_boxes:
		b=copy.deepcopy(box[1])
		b.shapes[0].remove_part(part_perc=partial_perc)
		ref_boxes_alt+=[(box[0],b)]
	ref_cylinders_alt=[]
	for cyl in ref_cylinders:
		c=copy.deepcopy(cyl[1])
		c.shapes[0].remove_part(part_perc=partial_perc)
		ref_cylinders_alt+=[(cyl[0],c)]
	return ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt

def add_error(error_perc, ref_spheres, ref_boxes, ref_cylinders):
	ref_spheres_alt=[]
	for sphere in ref_spheres:
		s=copy.deepcopy(sphere[1])
		s.shapes[0].set_error(error_percentage=error_perc)
		ref_spheres_alt+=[(sphere[0],s)]
	ref_boxes_alt=[]
	for box in ref_boxes:
		b=copy.deepcopy(box[1])
		b.shapes[0].set_error(error_percentage=error_perc)
		ref_boxes_alt+=[(box[0],b)]
	ref_cylinders_alt=[]
	for cyl in ref_cylinders:
		c=copy.deepcopy(cyl[1])
		c.shapes[0].set_error(error_percentage=error_perc)
		ref_cylinders_alt+=[(cyl[0],c)]
	return ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt

def	make_noisy(base_path,noisy_perc, ref_spheres, ref_boxes, ref_cylinders):
	if not os.access(base_path, os.W_OK):
		raise ValueError("Can't write on "+base_path)
	dir_path=os.path.join(base_path,"noisy")
	print("A noisy collection will be generated on",dir_path)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = add_noise(noisy_perc, ref_spheres, ref_boxes, ref_cylinders)
	make(dir_path,ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)

def	make_partial(base_path,partial_perc, ref_spheres, ref_boxes, ref_cylinders):
	if not os.access(base_path, os.W_OK):
		raise ValueError("Can't write on "+base_path)
	dir_path=os.path.join(base_path,"partial")
	print("A partial collection will be generated on",dir_path)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = remove_part(partial_perc, ref_spheres, ref_boxes, ref_cylinders)
	make(dir_path,ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)

def	make_error(base_path,error_perc, ref_spheres, ref_boxes, ref_cylinders, double_it=False):
	if not os.access(base_path, os.W_OK):
		raise ValueError("Can't write on "+base_path)
	if double_it:
		dir_path=os.path.join(base_path,"error_double")
		error_perc*=2.0
	else:
		dir_path=os.path.join(base_path,"error")
	print("A error collection will be generated on",dir_path)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = add_error(error_perc, ref_spheres, ref_boxes, ref_cylinders)
	make(dir_path,ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)

def	make_error_noisy(base_path,error_perc, noisy_perc, ref_spheres, ref_boxes, ref_cylinders):
	if not os.access(base_path, os.W_OK):
		raise ValueError("Can't write on "+base_path)
	dir_path=os.path.join(base_path,"error_noisy")
	print("A error+noisy collection will be generated on",dir_path)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = add_error(error_perc, ref_spheres, ref_boxes, ref_cylinders)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = add_noise(noisy_perc, ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)
	make(dir_path,ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)

def	make_partial_noisy(base_path,partial_perc, noisy_perc, ref_spheres, ref_boxes, ref_cylinders):
	if not os.access(base_path, os.W_OK):
		raise ValueError("Can't write on "+base_path)
	dir_path=os.path.join(base_path,"partial_noisy")
	print("A partial+noisy collection will be generated on",dir_path)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = remove_part(partial_perc, ref_spheres, ref_boxes, ref_cylinders)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = add_noise(noisy_perc, ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)
	make(dir_path,ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)

def	make_partial_error(base_path,partial_perc, error_perc, ref_spheres, ref_boxes, ref_cylinders):
	if not os.access(base_path, os.W_OK):
		raise ValueError("Can't write on "+base_path)
	dir_path=os.path.join(base_path,"partial_error")
	print("A partial+error collection will be generated on",dir_path)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = remove_part(partial_perc, ref_spheres, ref_boxes, ref_cylinders)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = add_error(error_perc, ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)
	make(dir_path,ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)

def	make_partial_noisy_error(base_path,partial_perc, noisy_perc, error_perc, ref_spheres, ref_boxes, ref_cylinders):
	if not os.access(base_path, os.W_OK):
		raise ValueError("Can't write on "+base_path)
	dir_path=os.path.join(base_path,"partial_noisy_error")
	print("A partial+noisy+error collection will be generated on",dir_path)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = remove_part(partial_perc, ref_spheres, ref_boxes, ref_cylinders)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = add_error(error_perc, ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)
	ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt = add_noise(noisy_perc, ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)
	make(dir_path,ref_spheres_alt, ref_boxes_alt, ref_cylinders_alt)

def add_zeros_to_100(count):
	s=str(count)
	if count<10:
		s='0'+s
	if count<100:
		s='0'+s
	return s

if __name__ == '__main__':
	parser = argparse.ArgumentParser(description='Generator of a collection of primitive shapes in the form of points clouds.')
	parser.add_argument('target_directory', metavar='target_directory', type=str, 
                    help='Target directory where the collection will be stored.')
	parser.add_argument('-all',  action='store_true',
                    help='All the collection types will be generated.')
	parser.add_argument('-seed', metavar='seed', type=int, nargs=1,
                    help='Set the seed for all the collection types generated.')
	parser.add_argument('-clean', action='store_true',
                    help='The collection with complete shapes with no error and no noise will be generated.')
	parser.add_argument('-noisy', metavar='noisy', type=float, nargs=1, 
                    help='The collection with complete shapes with no error and noise will be generated.')
	parser.add_argument('-error', metavar='error', type=float, nargs=1, 
                    help='The collection with complete shapes with error and no noise will be generated.')
	parser.add_argument('-partial', metavar='partial', type=float, nargs=1, 
                    help='The collection with partial shapes with no error and no noise will be generated.')
	args = parser.parse_args()

	if args.seed!=None:
		nr.seed(args.seed[0])
	else:
		nr.seed()
	
	# make reference shapes
	# make spheres
	count=0
	ref_spheres=[]
	r=sphere_pars["radius"][0]
	while r<=sphere_pars["radius"][1]:
		ref_spheres+=[("s_"+add_zeros_to_100(count),s_generator.Scene(s_generator.Sphere(general_pars["n_points"],r)))]
		r+=sphere_pars["radius"][2]
		count+=1
	# make boxes
	count=0
	ref_boxes=[]
	h=box_pars["height"][0]
	while h<=box_pars["height"][1]:
		w=h # box_pars["width"][0]
		while w<=box_pars["width"][1]:
			d=w #box_pars["depth"][0]
			while d<=box_pars["depth"][1]:
				b=s_generator.Box(general_pars["n_points"],h,w,d)
				b.rotate()
				ref_boxes+=[("b_"+add_zeros_to_100(count),s_generator.Scene(b))]
				d+=box_pars["depth"][2]
				count+=1
			w+=box_pars["width"][2]
		h+=box_pars["height"][2]
	# make cylinders
	count=0	
	ref_cylinders=[]
	h=cylinder_pars["height"][0]
	while h<=cylinder_pars["height"][1]:
		r=cylinder_pars["radius"][0]
		while r<=cylinder_pars["radius"][1]:
			c=s_generator.Cylinder(general_pars["n_points"],h,r)
			c.rotate()
			ref_cylinders+=[("c_"+add_zeros_to_100(count),s_generator.Scene(c))]
			r+=cylinder_pars["radius"][2]
			count+=1
		h+=cylinder_pars["height"][2]

	e=general_pars["default_error"]
	p=general_pars["default_partial"]
	n=general_pars["default_noise"]
	if args.error!=None:
		e=args.error[0]
	if args.partial!=None:
		p=args.partial[0]
	if args.noisy!=None:
		n=args.noisy[0]
	
	print("A number of",len(ref_spheres),"spheres,",len(ref_boxes),"boxes and",len(ref_cylinders),"cylinders will be generated for each collection type")
	# clean
	if args.all or args.clean:
		make_clean(args.target_directory, ref_spheres, ref_boxes, ref_cylinders)
		if not args.all:
			sys.exit(0)
	# noise
	if args.all or (args.noisy!=None and args.partial==None and args.error==None):
		make_noisy(args.target_directory,n, ref_spheres, ref_boxes, ref_cylinders)
	# error
	if args.all or (args.error!=None and args.noisy==None and args.partial==None):
		make_error(args.target_directory,e, ref_spheres, ref_boxes, ref_cylinders)
	# double error
	if args.all:
		make_error(args.target_directory,e, ref_spheres, ref_boxes, ref_cylinders, double_it=True)
	# error + noise
	if args.all or (args.noisy!=None and args.error!=None and args.partial==None):
		make_error_noisy(args.target_directory,e, n, ref_spheres, ref_boxes, ref_cylinders)
	# partial
	if args.all or (args.partial!=None and args.noisy==None and args.error==None):
		make_partial(args.target_directory,p, ref_spheres, ref_boxes, ref_cylinders)
	# partial + noise
	if args.all or (args.partial!=None and args.noisy!=None and args.error==None):
		make_partial_noisy(args.target_directory,p, n, ref_spheres, ref_boxes, ref_cylinders)
	# partial + error
	if args.all or (args.partial!=None and args.error!=None and args.noisy==None):
		make_partial_error(args.target_directory,p, e, ref_spheres, ref_boxes, ref_cylinders)
	# partial + noise + error
	if args.all or (args.partial!=None and args.noisy!=None and args.error!=None):
		make_partial_noisy_error(args.target_directory,p, n, e, ref_spheres, ref_boxes, ref_cylinders)
